import { createRouter, createWebHistory } from "vue-router";
import DetailsTodo from "../views/DetailsTodo.vue";
import TodoList from "../views/TodoList.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: "/", component: TodoList },
    { path: "/details/:id", component: DetailsTodo, props: true },
  ],
});

export default router;
