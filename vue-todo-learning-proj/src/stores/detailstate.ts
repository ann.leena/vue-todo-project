import { ref } from "vue";
import { defineStore } from "pinia";

export const useCounterStore = defineStore(
  "messager",
  () => {
    const message = ref("");
    const TODO = ref();
    return { message, TODO };
  },
  {
    persist: true,
  }
);
