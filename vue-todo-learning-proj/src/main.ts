import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";
// Vuetify
import "vuetify/styles";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import * as labs from 'vuetify/labs/components'

import axios from 'axios'
import VueAxios from 'vue-axios'

import "./assets/main.css";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";

const vuetify = createVuetify({  components: {
    ...labs,...components
  }, directives });

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const app = createApp(App);

app.use(pinia);
app.use(router);
app.use(vuetify);
app.use(VueAxios, axios)

app.mount("#app");
